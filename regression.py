import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression, LinearRegression


# read data from excel-file
# for description see separate file
akdData = pd.read_excel('./data/akd-data-2021-01-21.xlsx',
                names=["Nummer","KDIGO","age","sex","ward","LOS","CKD","dialysis","mort","charlson","ventilation","sepsis","mortdia","AKD","pFl","pB","pG","pAKI","Stadium0","mortdiaAKD","creatinine","diaAKD","Delta", "Fl","B","G"
                    ],
                dtype="Int64",
#                na_values=["9999"],    #doesn't work
                index_col=0
        )

akdData = akdData.drop(columns=["mort", "mortdia", "diaAKD", "mortdiaAKD"])

akdData.replace(
            9999, pd.NA, inplace=True
        )
akdData.dropna(inplace=True)

# assign correct data types
# akdData = akdData.astype(
#            dtype={"KDIGO":bool, "age":"Int64", "sex":bool, "ward":"Int64" ,"LOS":"Int64" ,"CKD":bool , "dialysis": bool,  "charlson": "Int64", "ventilation": bool, "sepsis":bool , "AKD":bool , "pFl":bool , "pB":bool , "pG":bool , "pAKI":bool , "Stadium0":bool , "creatinine":"Int64" , "Delta":bool
#                },
#        )

akdData = akdData.astype("int64")

akd_feature = akdData[["KDIGO","age","sex","ward","LOS","CKD","dialysis","charlson","ventilation","sepsis","pFl","pB","pG","pAKI","Stadium0","creatinine","Delta"
    ]]
akd_label = akdData[['AKD']]


# test perfect fit (all data) with standard method
clf = LogisticRegression(
        solver='saga',
        penalty="elasticnet",
        l1_ratio=0.5,
        max_iter=10000,
        random_state=12
        )
clf.fit(akd_feature, akd_label.to_numpy().ravel())
print(
    clf.score(akd_feature, akd_label.to_numpy().ravel())
    )

# train and test data
from sklearn.model_selection import train_test_split
akd_feature_train, akd_feature_test, akd_label_train, akd_label_test = train_test_split(akd_feature, akd_label, test_size = 0.25, random_state = 42)
clf = LogisticRegression(
        solver='saga',
        penalty="elasticnet",
        l1_ratio=0.5,
        max_iter=10000,
        random_state=12
        )
clf.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
print(
    clf.score(akd_feature_test, akd_label_test.to_numpy().ravel())
    )

# problem: model says always "no"
# Accuracy is same as "by chance"


from sklearn.metrics import (precision_recall_curve,
                              PrecisionRecallDisplay)
precision, recall, _ = precision_recall_curve(akd_label_test, clf.predict(akd_feature_test))
disp = PrecisionRecallDisplay(precision=precision, recall=recall)
disp.plot()

plt.show()


import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.metrics import PrecisionRecallDisplay
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
X, y = make_classification(random_state=0)
X_train, X_test, y_train, y_test = train_test_split(
         X, y, random_state=0)
clf = LogisticRegression()
clf.fit(X_train, y_train)
LogisticRegression()
y_pred = clf.predict_proba(X_test)[:, 1]
PrecisionRecallDisplay.from_predictions(
    y_test, y_pred)
plt.show()