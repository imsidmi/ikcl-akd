import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression, LinearRegression


# read data from excel-file
# for description see separate file
akdData = pd.read_excel('./data/akd-data-2021-01-21.xlsx',
                names=["Nummer","KDIGO","age","sex","ward","LOS","CKD","dialysis","mort","charlson","ventilation","sepsis","mortdia","AKD","pFl","pB","pG","pAKI","Stadium0","mortdiaAKD","creatinine","diaAKD","Delta", "Fl","B","G"
                    ],
                dtype="Int64", #pandas dtype with nan-support
#                na_values=["9999"],    #doesn't work
                index_col=0
        )

akdData = akdData.drop(columns=["mort", "mortdia", "diaAKD", "mortdiaAKD"])

akdData.replace(
            9999, pd.NA, inplace=True
        )
akdData.dropna(inplace=True)

# assign correct data types
# akdData = akdData.astype(
#            dtype={"KDIGO":bool, "age":"Int64", "sex":bool, "ward":"Int64" ,"LOS":"Int64" ,"CKD":bool , "dialysis": bool,  "charlson": "Int64", "ventilation": bool, "sepsis":bool , "AKD":bool , "pFl":bool , "pB":bool , "pG":bool , "pAKI":bool , "Stadium0":bool , "creatinine":"Int64" , "Delta":bool
#                },
#        )

akdData = akdData.astype("int64")

akd_feature = akdData[["KDIGO","age","sex","ward","LOS","CKD","dialysis","charlson","ventilation","sepsis","pFl","pB","pG","pAKI","Stadium0","creatinine","Delta"
    ]]
akd_label = akdData[['AKD']]


# train and test data
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import neural_network
from sklearn.metrics import confusion_matrix
akd_feature_train, akd_feature_test, akd_label_train, akd_label_test = train_test_split(akd_feature, akd_label, test_size = 0.25, random_state = 42)
import matplotlib.pyplot as plt

for h in [4,5, 6, 10, 20, 60, 70, 80]:
    mlp = neural_network.MLPRegressor(hidden_layer_sizes=(h),
        #verbose=False,
        batch_size=20, max_iter=5000, random_state=12)
    mlp.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
    print(
        #mlp.score(akd_feature_test, akd_label_test.to_numpy().ravel())
        np.array([mlp.predict(akd_feature_test) > 0.5])
    )

for h in [4,5, 6, 10, 20, 60, 70, 80]:
    mlp = neural_network.MLPRegressor(
        hidden_layer_sizes=(h),
        activation="logistic",
        batch_size=20,
        max_iter=5000,
        random_state=12)
    mlp.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
    print("h: ", h)
    print("Train confusion Matrix:")
    print(confusion_matrix(akd_label_train,np.array([ (0 if x<0.5 else 1) for x in mlp.predict(akd_feature_train)]) ))
    print("Test confusion Matrix:")
    confmat = confusion_matrix(akd_label_test,np.array([ (0 if x<0.5 else 1) for x in mlp.predict(akd_feature_test)]) )
    print(confmat)

from sklearn.metrics import classification_report

for h in [4,5, 6, 10, 20, 60, 70, 80]:
    mlp = neural_network.MLPRegressor(
        hidden_layer_sizes=(h),
        activation="logistic",
        batch_size=20,
        max_iter=5000,
        random_state=12)
    mlp.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
    print("h: ", h)
    #print("Train performance:",
    #    )
    #print(classification_report(akd_label_train,np.array([ (0 if x<0.5 else 1) for x in mlp.predict(akd_feature_train)]) ))
    print("Test performance:", classification_report(akd_label_test,np.array([ (0 if x<0.5 else 1) for x in mlp.predict(akd_feature_test)]) ))


for h in [20, 60, 70, 80, 90, 100]:
    mlp = neural_network.MLPClassifier(
        hidden_layer_sizes=(h),
        activation="relu",
        batch_size=20,
        max_iter=5000,
        random_state=12)
    mlp.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
    print("h: ", h)
    #print("Train performance:", classification_report(akd_label_train, mlp.predict(akd_feature_train)))
    #print("Test performance:", classification_report(akd_label_test, mlp.predict(akd_feature_test)))
    print(confusion_matrix(akd_label_test, mlp.predict(akd_feature_test)))

# bei 100 geht es so langsam besser, falsch positiv ist mir eigentlich lieber (lieber falsche Warnung als nicht entdeckt


mlp = neural_network.MLPClassifier(
    hidden_layer_sizes=(100),
    activation="relu",
    batch_size=20,
    max_iter=5000,
    #n_iter_no_change=50
    random_state=12)
mlp.fit(akd_feature_train, akd_label_train.to_numpy().ravel())
print("h: ", h)
print(confusion_matrix(akd_label_test, mlp.predict(akd_feature_test)))
precision_recall_fscore_support(akd_label_test,mlp.predict(akd_feature_test) )[2].mean()

# mache die seltenen Events künstlich häufiger
# (den Loss anpassen wäre besser)
mlp = neural_network.MLPClassifier(
    hidden_layer_sizes=(100),
    activation="relu",
    batch_size=20,
    max_iter=5000,
    #n_iter_no_change=50
    random_state=12)
new_feature_train = pd.concat((akd_feature_train, akd_feature_train[akd_label_train["AKD"] == 1],akd_feature_train[akd_label_train["AKD"] == 1]))
new_label_train = pd.concat( (akd_label_train, akd_label_train[akd_label_train["AKD"] == 1], akd_label_train[akd_label_train["AKD"] == 1]) )
mlp.fit(new_feature_train, new_label_train.to_numpy().ravel())
print("h: ", h)
print(confusion_matrix(akd_label_test, mlp.predict(akd_feature_test)))
print("Test performance:", classification_report(akd_label_test,mlp.predict(akd_feature_test) ))

from sklearn.metrics import precision_recall_fscore_support

# Besseres Gütemaß?
precision_recall_fscore_support(akd_label_test,mlp.predict(akd_feature_test) )[2].mean()
