import pandas as pd

# read data from excel-file
# for description see separate file
akdData = pd.read_excel('./data/akd-data-2021-01-21.xlsx',
                names=["Nummer","KDIGO","age","sex","ward","LOS","CKD","dialysis","mort","charlson","ventilation","sepsis","mortdia","AKD","pFl","pB","pG","pAKI","Stadium0","mortdiaAKD","creatinine","diaAKD","Delta", "Fl","B","G"
                    ],
                dtype={"KDIGO":"boolean", "age":"Int64", "sex":"Int64", "ward":"Int64" ,"LOS":"Int64" ,"CKD":"boolean" , "dialysis": "boolean", "Mort": "boolean", "charlson": "Int64", "ventilation": "boolean", "sepsis":"boolean" , "mortdia":"boolean" , "AKD":"boolean" , "pFl":"boolean" , "pB":"boolean" , "pG":"boolean" , "pAKI":"boolean" , "Stadium0":"boolean" , "mortdiaAKD":"boolean" , "creatinine":"Int64" , "diaAKD":"boolean" , "Delta":"boolean" , "Fl":"boolean" , "B":"boolean" , "G":"boolean" },
                true_values=["1"],
                false_values=["0"],
                na_values=["9999"],
                index_col=0
        )
# applymap(bool) fehlt, weil boolean eine 1 nicht als True interpretiert

akdData = akdData.drop(columns=["mort", "mortdia", "diaAKD", "mortdiaAKD"])

akdData.replace(
            9999, pd.NA, inplace=True
        )
akdData.dropna(inplace=True)

# assign correct data types
akdData = akdData.astype(
            dtype={"KDIGO":bool, "age":"Int64", "sex":bool, "ward":"Int64" ,"LOS":"Int64" ,"CKD":bool , "dialysis": bool,  "charlson": "Int64", "ventilation": bool, "sepsis":bool , "AKD":bool , "pFl":bool , "pB":bool , "pG":bool , "pAKI":bool , "Stadium0":bool , "creatinine":"Int64" , "Delta":bool
                },
        )

akd_feature = akdData[["KDIGO","age","sex","ward","LOS","CKD","dialysis","charlson","ventilation","sepsis","pFl","pB","pG","pAKI","Stadium0","creatinine","Delta"
    ]]
akd_label = akdData[['AKD']]

