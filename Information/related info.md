### Beschreibung aus Mails

Im Anhang nun nochmal eine kurze Erklärung zu den Spalten. Wie gesagt, der Datensatz reduziert sich auf AKD 0 und 1, 9999 fliegt raus, also eher 6000 Datensätze statt 14000.
Die Fragestellung bezieht sich auf die Vorhersagbarkeit von AKD. Das gibt es in der Literatur wirklich bisher super wenig, bis fast nichts. Ist definitiv eine Short communication aus medizinischer Sicht, auch wenn herauskommt, dass KDIGO positiv nicht verbessert werden kann.
Falls man andere Variablen dazu holt wäre es auch interessant, ob einmal mit dem Block (Sepsis, Ventilation, dialysis = schweriegende Ereignisse während aufenthalt [könnte man auch zusammenfassen]) und einmal ohne diesen Block.

Das ist auf die schnelle  der Komplette Datensatz.
Wir würden nur auf AKD gehen (also Endpunkte mort, dia, diaakd etc. würde rausfliegen), somit alle die bei AKD 9999 haben fliegen raus.
 
Sind jetzt nicht dramatisch viele Variablen (mit mehr Zeit könnte ich die eine oder andere mehr generieren), aber bei wenig Zeit sind wenige Variablen vielleicht ja ganz gut.
Durch die Variablen würde ich Dich am besten Telefonisch durchsprechen.
Wichtig entweder wir nehmen pFl oder Fl, pB oder B etc. beides geht nicht. Würde bevorzugen pF l zu nehmen.
KDIGO ist die wichtigste Variable für AKD, das ist klar. 
Es geht als erste darum ob man mit den übrigen Variablen die Vorhersage verbessern kann auf AKD.

