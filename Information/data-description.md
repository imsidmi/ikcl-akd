| KDIGO			|Wichtigste Variable für AKD, akutes Nierenversagen|
|---------------|--------------------------------------------------|
| age			| Variable |
| sex			| Variable |
| ward			| Aufnahmestation, habe es zu Gruppen zusammengefasst und numeriert |
| LOS			| Aufenthaltsdauer, mmh, sollte man eher nicht als Variable nehmen, da ja erst hinterher bekannt. |
| CKD			| Variable (nierenvorerkrankung) |
| dialysis		| Diaylsebehandlung während aufenthalt, Könnte man auch als Variable verwenden |
| Mort			| Versterben während aufenthalt |
| charlson		| Anzahl Begleiterkrankungen, d.h. je höher der Index desto mehr Begleiterkrankungen |
| ventialtion	| Beatmung während Aufenthalt |
| sepsis		| Sepsis während Aufenthalt |
| mortdia		| Kombination aus Mortalität und Dialyse, kann raus |
| AKD			| ENDPUNKT = Akute Nierenerkrankung bei Krankenhausentlassung |
| pFl			| Alarmsystem 1 nur positiv wenn Alarm vor KDIGO |
| pB			| Alarmsystem 2 nur positiv wenn Alarm vor KDIGO |
| pG			| Alarmsystem 3 nur positiv wenn Alarm vor KDIGO |
| pAKI			| Eines der Alarmsystem 1-3 ist positiv |
| Stadium0		| Stadium des KDIGO (ist also nur bei KDIGO=1 zu bewerten) |
| mortidaAKD	| 3facher Endpunkt kann raus |
| creatinine	| Aufnahmekreatinin, Variable |
| diaAKD		| Kombiendpunkt, brauchen wir auch nicht. |
| Delta			| Alarmsystem 4 (hier kein pDelta, da nie positiv vor KDIGO) |
| Fl			| Alarmsystem 1 |
| B				| Alarmsystem 2 |
| G				| Alarmsystem 3 |
